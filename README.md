**RUN:**

```
docker run \
    -v /opt/keepalived/keepalived.conf:/etc/keepalived/keepalived.conf \
    --name=keepalived \
    --privileged=true \
    --net=host \
    -t <image_name:tag> \
    /usr/sbin/keepalived -f /etc/keepalived/keepalived.conf --dont-fork --log-console
```

**Systemd:**

```
[Unit]
Description=keepalived

[Service]
ExecStartPre=-/usr/bin/docker stop keepalived
ExecStartPre=-/usr/bin/docker rm keepalived
ExecStartPre=/sbin/modprobe -s ip_vs
ExecStartPre=/usr/bin/docker pull <image_name:tag>
ExecStartPre=/bin/sh -c "/usr/sbin/sysctl -w net.ipv4.ip_nonlocal_bind=1"
ExecStartPre=/bin/sh -c "/usr/sbin/sysctl -w net.ipv4.ip_forward=1"
ExecStart=/usr/bin/docker run --name keepalived -v /keepalived.conf:/etc/keepalived/keepalived.conf \
-v /sbin/modprobe:/sbin/modprobe -v /lib/modules:/lib/modules \
-v /opt/bin/keepalived/notify.sh:/opt/bin/notify.sh --privileged=true --net=host \
-t <image_name:tag> /usr/sbin/keepalived -f /etc/keepalived/keepalived.conf --dont-fork --log-console
ExecStop=/bin/sh -c "/usr/sbin/sysctl -w net.ipv4.ip_nonlocal_bind=0"
ExecStop=/usr/bin/docker stop keepalived
```

**Example keepalived.conf**

```
vrrp_instance VI_1 {
  interface eth0
  state MASTER
  virtual_router_id 51
  priority <priority_int> # 101 on master, 100 on backups

  unicast_peer {
    <peer ip>
  }
  # not using unicast peers in our setup, as we want to dynamically assign members to VRRP network
  virtual_ipaddress {
    <vip ip>
  }
  # optional
  authentication {
    auth_type PASS
    auth_pass <password>
  }
}
```
